import { inbox } from "file-transfer";
import { listDirSync, unlinkSync, readFileSync } from "fs";
import * as logging from "../../common/logging";

let receiverCallback;

// Process the inbox queue for files, and read their contents as text
async function processAllFiles() {
  logging.info(`processAllFiles`);
  let fileName;
  while (fileName = inbox.nextFile()) {
    logging.info(`/private/data/${fileName} is now available`);
    processFile(fileName)
  }
}

function processFile(fileName) {
    try {
        var data = readFileSync(fileName, "cbor");
        receiverCallback(data);
    }
    catch (error) {
        logging.error(`error reading ${fileName}`);
    }
}

function clearPrivateData(startMatch) {
    const listDir = listDirSync("/private/data");
    do {
        const dirIter = listDir.next();
        if (dirIter.done) {
            break;
        };
        if (dirIter.value.substr(0,startMatch.length) == startMatch) {
            logging.info(`deleteing ${dirIter.value}`);
            unlinkSync(dirIter.value);
        } else {
            logging.info(`skipping ${dirIter.value}`);
        }
    } while (true);    
}

export function initialize(callback) {
    logging.info(`receiver init`);
    receiverCallback = callback;

    clearPrivateData("_wrist_list_");

    // Process new files as they are received
    inbox.addEventListener("newfile", processAllFiles);

    // Also process any files that arrived when the app wasn’t running
    processAllFiles()
}
  
