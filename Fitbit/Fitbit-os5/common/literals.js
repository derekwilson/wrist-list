// app version
export const APP_VERSION = "2.1 step1";

export const JS_MEMORY_ALERT_LEVEL = 60000;

// analytics
export const ANALYTICS_TRACKING_ID = "UA-149964332-2";
export const ANALYTICS_DATA_SOURCE = "wrist-list-fitbit-" + APP_VERSION;

export const ANALYTICS_CATEGORY_LAUNCH = "Launch";

export const ANALYTICS_CATEGORY_UPGRADE = "Upgrade";

export const ANALYTICS_CATEGORY_LIST = "List";
export const ANALYTICS_ACTION_RECEIVE = "Receive";
export const ANALYTICS_ACTION_TOGGLE = "Toggle";

export const ANALYTICS_CATEGORY_ERROR = "Error";
export const ANALYTICS_ACTION_MEMORY = "Memory";
export const ANALYTICS_ACTION_LOAD_SETTINGS = "LoadSettings";

// settings
export const SETTING_VERSION = "version";
export const SETTING_SEND = "send";
export const SETTING_SEPARATOR = "sep";
export const SETTING_TODO_1 = "todo1";
export const SETTING_SORT = "sort";
export const SETTING_DELETE_CHECKED = "delchecked";

// defaults
export const DEFAULT_SEND = 1;
export const MAX_SEND = 100;
export const DEFAULT_SORT = true;
export const DEFAULT_DELETE_CHECKED = true;
export const DEFAULT_SEPARATOR =  [{"name":"[x]","value":"\\[x]"},{"name":"[ ]","value":"\\[ ]"}]

export const SEP_UNCHECK = "[ ]";
export const SEP_CHECK = "[x]";
// these will be used in a regex so we need to escape any special regex chars
export const SEP_UNCHECK_VALUE = "\\[ ]";
export const SEP_CHECK_VALUE = "\\[x]";
export const SEP_COMMA = ",";
export const SEP_MINUS = "-";
export const SEP_PLUS = "+";
export const SEP_PLUS_VALUE = "\\+";
export const SEP_STAR = "*";
export const SEP_STAR_VALUE = "\\*";
export const SEP_SEMICOLON = ";";

export const SEP_TAB = "\t";
export const CHECKED_MARKER = "<x>";
export const MAX_ITEM_LENGTH = 30;
