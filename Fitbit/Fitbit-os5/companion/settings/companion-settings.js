import * as logging from "../../common/logging";
import * as literals from "../../common/literals";
import * as xfer from "../xfer/sender";
import { settingsStorage } from "settings";

export function initialize() {
  setDefaultSetting(literals.SETTING_SEND, literals.DEFAULT_SEND, true);
  setDefaultSetting(literals.SETTING_SORT, literals.DEFAULT_SORT, true);
  setDefaultSetting(literals.SETTING_DELETE_CHECKED, literals.DEFAULT_DELETE_CHECKED, true);
  setDefaultSetting(literals.SETTING_SEPARATOR, literals.DEFAULT_SEPARATOR, true);

  settingsStorage.addEventListener("change", evt => {
    logging.debug(`setting ${evt.key}: ${evt.oldValue} -> ${evt.newValue}`)
    if (evt.key == literals.SETTING_SEND) {
      sendList(1, literals.SETTING_TODO_1);
    }
  });
}

function setDefaultSetting(key, value, stringify) {
  let extantValue = settingsStorage.getItem(key);
  if (extantValue === null) {
    logging.debug(`need to set default for ${key} to ${value}`);
    if (stringify) {
      settingsStorage.setItem(key, JSON.stringify(value));
    } else {
      settingsStorage.setItem(key, value);
    }
  }
}

function isCheckedSeparator(sep) {
  // currently only "[x]" and "+"
  return (
    sep === literals.SEP_CHECK_VALUE ||
    sep === literals.SEP_PLUS_VALUE
  );
}

function replaceAll(text, sep, replaceText, shouldDeleteCheckedItems) {
  if (text == null) {
    return "";
  }
  let replaceTextCopy = replaceText;
  if (shouldDeleteCheckedItems) {
    logging.debug("should be deleting checked items");
    if (isCheckedSeparator(sep)) {
      // if we want to remove checked items need to mark them
      logging.debug("adding marker");
      replaceTextCopy = replaceText + literals.CHECKED_MARKER;
    }
  }
  let allSep = new RegExp(sep, "g");
  const newText = text.replace(allSep,replaceTextCopy);
  logging.debug(`replace ${sep} to ${replaceTextCopy} (${shouldDeleteCheckedItems}) produced ${newText}`);
  return newText;
}

function truncateItem(itemText, maxLength) {
  let newText = itemText.trim();
  if (newText.length > maxLength) {
    newText = newText.substring(0,maxLength);
  }
  return newText;
}

function formatTimestamp(now) {
  //const options = { year: 'numeric', month: 'short', day: '2-digit', weekday: 'short', hour: '2-digit', minute : '2-digit'};
  const options = { month: 'short', day: '2-digit', weekday: 'short', hour: '2-digit', minute : '2-digit'};
  const dtf = new Intl.DateTimeFormat('en', options); 
  return dtf.format(now); 
}

function sendList(slotNumber, listSettingName) {
  let settingSep = settingsStorage.getItem(literals.SETTING_SEPARATOR);
  let settingList = settingsStorage.getItem(listSettingName);
  // make sure we get booleans back rather than strings
  let sortToggle = settingsStorage.getItem(literals.SETTING_SORT) == "true";
  let deleteCheckedToggle = settingsStorage.getItem(literals.SETTING_DELETE_CHECKED) == "true";

  logging.debug(`sep ${settingSep}`);
  logging.debug(`original ${settingList}`);
  logging.debug(`sort ${sortToggle}, delChecked ${deleteCheckedToggle}`);

  let originalList = "";
  if (settingList == null) {
    originalList = "";
  } else {
    originalList = JSON.parse(settingList).name;
  }

  let sep = null;
  if (settingSep != null) {
    sep = JSON.parse(settingSep);
  }
  if (Array.isArray(sep)) {
    for (var index = 0; index < sep.length; index++) {
      originalList = replaceAll(originalList, sep[index].value, literals.SEP_TAB, deleteCheckedToggle);
    }
  } else {
    if (sep == null) {
      sep = literals.SEP_COMMA;
    } else {
      sep = sep.name;
    }
    originalList = replaceAll(originalList, sep, literals.SEP_TAB, deleteCheckedToggle);
  }
  let finalList = originalList.split(literals.SEP_TAB);
  // do the heavy lifting where we have processor power - on the phone
  finalList = finalList.map(s => truncateItem(s, literals.MAX_ITEM_LENGTH));
  finalList = finalList.filter(s => s != "");
  if (deleteCheckedToggle) {
    // remove any we marked earlier
    finalList = finalList.filter(s => !s.startsWith(literals.CHECKED_MARKER));
  }
  if (sortToggle) {
    finalList.sort(function (a, b) {
      return a.toLowerCase().localeCompare(b.toLowerCase());
    });
  }
  logging.debug(`list ${finalList.length}`);
  var data = {
    reset: true,
    slot: slotNumber, 
    timestamp: formatTimestamp(Date.now()),
    list: finalList
  };
  logging.debug(`object ${JSON.stringify(data)}`);
  xfer.sendDataToDevice(data);

}


