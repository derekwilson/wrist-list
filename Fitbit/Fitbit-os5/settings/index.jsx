import * as literals from "../common/literals";
import * as logging from "../common/logging";

logging.set_logging_level(logging.DEFAULT_LOG_LEVEL);
console.log("Opening wrist-list settings page");

function settingsComponent(props) {
  return (
    <Page>
      <Section title="wrist-list">
        <Text bold align="center">v{literals.APP_VERSION}</Text>
      </Section>
      <Section title="Separators">
        <AdditiveList
            title="List of separators"
            settingsKey={literals.SETTING_SEPARATOR}
            maxItems="5"
            addAction={
            <TextInput
              title="Add Separator"
              label="Add Seperator"
              placeholder="Type something"
              action="Add Item"
              onAutocomplete={(value) => {
                const autoValues = [
                  { name: literals.SEP_CHECK, value: literals.SEP_CHECK_VALUE },
                  { name: literals.SEP_UNCHECK, value: literals.SEP_UNCHECK_VALUE },
                  { name: literals.SEP_COMMA, value: literals.SEP_COMMA },
                  { name: literals.SEP_MINUS, value: literals.SEP_MINUS },
                  { name: literals.SEP_PLUS, value: literals.SEP_PLUS_VALUE },
                  { name: literals.SEP_STAR, value: literals.SEP_STAR_VALUE },
                  { name: literals.SEP_SEMICOLON, value: literals.SEP_SEMICOLON }
                ];
//                return autoValues.filter((option) => option.name.startsWith(value));
                return autoValues;
              }}
            />
            }
          />
      </Section>
      <Section title="Todo List">
        <TextInput
          label="List Items"
          title="Separated List of Items"
          type="text"
          settingsKey={literals.SETTING_TODO_1}
        />
      </Section>
      <Section title="Send">
        <Toggle
            settingsKey={literals.SETTING_SORT}
            label="Sort alphabetically"
        />
        <Toggle
            settingsKey={literals.SETTING_DELETE_CHECKED}
            label="Remove checked items"
        />
        <Text>Ensure wrist-list is running on your device before sending.</Text>
        <Button
          label="Send to device"
          onClick={() => {
            logging.debug("Send Clicked");
            let currentValue = literals.DEFAULT_SEND;
            try {
              currentValue = parseInt(props.settingsStorage.getItem(literals.SETTING_SEND)) + 1;
              if (currentValue > literals.MAX_SEND) {
                currentValue = literals.DEFAULT_SEND;
              }
            } catch (e) {
              currentValue = literals.DEFAULT_SEND
            }
            props.settingsStorage.setItem(literals.SETTING_SEND, currentValue + "");
          }}
        />
      </Section>
      <Text bold align="center">Licenses</Text>
      <Section title="Design Asset License">
        <Text>Copyright (c) Fitbit, Inc.</Text>
        <Link source="https://github.com/Fitbit/sdk-design-assets/blob/master/LICENCE.txt">https://github.com/Fitbit/sdk-design-assets/blob/master/LICENCE.txt</Link>
      </Section>
      <Section title="SDK Moment">
        <Text>MIT License Copyright (c) 2018 Fitbit, Inc</Text>
        <Link source="https://github.com/Fitbit/sdk-moment/blob/master/LICENSE">https://github.com/Fitbit/sdk-moment/blob/master/LICENSE</Link>
      </Section>
    </Page>
  );
}

registerSettingsPage(settingsComponent);
