# wrist-list

A todo list for Fitbit OS devices

## Requirement
### Local
- Node.js 8.+

(I used Node v10.16.0)

### Fitbit Studio
- None

## Build
### Local

```sh
$ git clone https://bitbucket.org/derekwilson/wrist-list.git
$ cd wrist-list
wrist-list$ npm install
wrist-list$ npx fitbit
fitbit$ build
fitbit$ install
```

### Fitbit Studio
```sh
$ git clone https://bitbucket.org/derekwilson/wrist-list.git
```

