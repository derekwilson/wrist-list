Completely free todo list for Fitbit OS watches, take a todo list from your phone and put it on your wrist

Example for Google Keep
1. Open Keep on your phone, goto the main screen that displays your notes
2. Long press on a list
3. From the overflow menu select "Send"
4. On the share menu, either copy it to the clipboard or send it to an app where you can copy the list to the clipboard
5. Open the Fitbit app on your phone
6. In the Apps section goto the settings page for wrist-list
7. Ensure the separators are "[ ]" and "[x]"
8. Select the todo list, clear any content, long press and paste the clipboard. It should look something like this "[ ] milk [ ] bread [x] cheese"
9. Open wrist-list on your Fitbit device
10. Select "send to device"

wrist-list
- List is persisted even when you exit the app
- Choice of separators: "[ ]", "[x]", ",", ";", "+", "-", "*"
- Remove checked items when sending the list: "[x]", "+"

more details
http://derekwilson.net/blog/2020/05/25/wrist-list-released
