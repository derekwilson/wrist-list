import document from "document";

import { me } from "appbit";
import { vibration } from "haptics";

import * as literals from "../common/literals";
import * as logging from "../common/logging";
import * as emptyObjects from "../common/objects";
import * as deviceUtil from "./device-utils";
import * as deviceSettings from "./device-settings";
import * as analytics from "./analytics";
import * as xfer from "./xfer/receiver";
import * as simpleClock from "./simple/clock";

// settings for debugging
//logging.set_logging_level(logging.LOGGING_LEVEL_DEBUG);
//analytics.initialize(true, true);

// settings for release
logging.set_logging_level(logging.LOGGING_LEVEL_INFO);
analytics.initialize(true, true);

deviceUtil.logMemoryStats("init");
analytics.sendAnalyticsEvent(literals.ANALYTICS_CATEGORY_LAUNCH, literals.APP_VERSION);

// this does not appear to work in the simulator - lets hope its OK in a device
// see https://community.fitbit.com/t5/SDK-Development/me-appTimeoutEnabled-will-not-be-set/td-p/3079978
me.appTimeoutEnabled = false;
logging.debug(`Timeout = ${me.appTimeoutEnabled}`);

/////////////////////////
// main screen
/////////////////////////
const mainScreen = document.getElementById("main-screen");
const noItemsUi = mainScreen.getElementById('no-items')
const noConnectionUi = mainScreen.getElementById('no-connection')
const mainTitleBanner = mainScreen.getElementById("heading-banner");
const mainTitleLeft = mainScreen.getElementById("heading-text-top-left");
const mainTitleRight = mainScreen.getElementById("heading-text-top-right");
const tileList = document.getElementById("main-list");

const selectedBackgroundColour = "#000000";
const selectedTextColour = "green";
const notSelectedBackgroundColour = "#000000";
const notSelectedTextColour = "#FFFFFF";

// the todo list
let todoList_1 = emptyObjects.default_list();

function receiveDataSet(reset, list, listSlot) {
  // always set the top item as an empty header to slide behind the header bar
  listSlot.list = [{
    type: "header",
    text: "",
    selected: false
  }];
  for (var index = 0; index<list.length; index++) {
      if (list[index] != null && list[index] != "") {
        listSlot.list.push({
          type: "item",
          text: list[index],
          selected: false
        });
      }
  }
}

function setupTileColours(bg, textArea, selected) {
  bg.style.fill = selected ? selectedBackgroundColour : notSelectedBackgroundColour;
  textArea.style.fill = selected ? selectedTextColour : notSelectedTextColour;
}

function setupTileList(data) {
    tileList.delegate = {
      getTileInfo: (index) => {
        const poolType = (data.list[index].type === "header") ? "header-pool" : "item-pool";
        return {
          type: poolType,
          data: data.list[index],
        };
      },
      configureTile: (tile, info) => {
        logging.debug(`configureTile ${info.data.text}, ${info.data.selected}`);
        if (info.data.type === "header") {
          tile.getElementById("item-header-text").text = info.data.text;
        } else {
          //for (var index = 0; index<tile.children.length; index++) {
          //  logging.debug(`child ${index}, ${tile.children[index].id}, ${tile.children[index].class}`);
          //}

          const bg = tile.getElementById("item-background");
          const checkBoxTile = tile.getElementById("item-text");
          const textArea = checkBoxTile.getElementById("header");

          checkBoxTile.text = info.data.text;
          setupTileColours(bg,textArea,info.data.selected);

          // check the box - a funky approach to force a redraw and resize if the text is multi line
          if (info.data.selected) {
            checkBoxTile.value = 1;
          } else {
            checkBoxTile.value = 1;   // force redraw
            checkBoxTile.value = 0;
          }

          checkBoxTile.onclick = (evt) => {
            logging.debug(`tile click ${info.data.text} :: ${JSON.stringify(evt)}`);
            info.data.selected = !info.data.selected;
            analytics.sendAnalyticsEventAndLabel(
              literals.ANALYTICS_CATEGORY_LIST, 
              literals.ANALYTICS_ACTION_TOGGLE,
              (info.data.selected ? "on" : "off")
            );
            setupTileColours(bg,textArea,info.data.selected);
            updateTitleBar(data);
          };
        }
      }
    };
}

function refreshList(data) {
  logging.info(`refreshList length ${data.list.length} items`);
  tileList.length = data.list.length;
  checkEmptyPage();
  updateTitleBar(data);
}

function setAllItemsSelectedValue(listSlot, selectedValueToSet) {
  for (var index = 0; index<listSlot.list.length; index++) {
    // only set the items, not the headers
    if (listSlot.list[index].type == "item") {
      listSlot.list[index].selected = selectedValueToSet;
    }
  }
  refreshList(listSlot);
}

function updateList(data, listSlot) {
  //logging.debug("receiverCallback " + JSON.stringify(data));
  if (data.list) {
    logging.info(`receiverCallback adding ${data.list.length} items`);
    analytics.sendAnalyticsEventLabelAndValue(
      literals.ANALYTICS_CATEGORY_LIST, 
      literals.ANALYTICS_ACTION_RECEIVE,
      data.list.length,
      data.list.length
      );
    receiveDataSet(data.reset, data.list, listSlot);
    listSlot.timestamp = data.timestamp;    // send from companion
    //logging.debug("list slot " + JSON.stringify(listSlot));
    setupTileList(listSlot);
    refreshList(listSlot);
  }
}

function checkEmptyPage() {
  // there will always be a header
  if (tileList.length > 1) {
    noItemsUi.style.display = 'none'
    tileList.style.display = 'inline'
  } else {
    noItemsUi.style.display = 'inline'
    tileList.style.display = 'none'
  }
}

function updateTitleBar(listSlot) {
  logging.debug(`updateTitleBar ${listSlot.list.length}`);
  let selectedCount = 0;
  let totalCount = 0;
  if (tileList.length == 0) {
    mainTitleLeft.text = `v${literals.APP_VERSION}`;
  } else {
    for (var index = 0; index<listSlot.list.length; index++) {
      if (listSlot.list[index].selected) {
        selectedCount = selectedCount + 1;
      }
      // only count the items, not the headers
      if (listSlot.list[index].type == "item") {
        totalCount = totalCount + 1;
      }
    }
    mainTitleLeft.text = `${selectedCount} of ${totalCount}`;
  }
}

/////////////////////////
// about screen
/////////////////////////
const aboutScreen = document.getElementById("about-screen");
const aboutTitle = aboutScreen.getElementById("about-title");
const aboutCopy = aboutScreen.getElementById("about-copy");
const resetButton = aboutScreen.getElementById("reset-button");

aboutTitle.onclick = function(evt) {
  logging.debug("onclick about title");
  showMainScreen();
}

aboutCopy.onclick = function(evt) {
  logging.debug("onclick about copy");
  showMainScreen();
}

resetButton.onclick = function(evt) {
  logging.debug("onclick reset");
  //todoList_1 = emptyObjects.default_list();
  //refreshList(todoList_1);
  setAllItemsSelectedValue(todoList_1,false);
  deviceSettings.save(todoList_1);
  showMainScreen();
}

////////////////////////////////
// navigation
////////////////////////////////

mainTitleBanner.onclick = function(evt) {
    logging.debug("title banner onclick");
    showAboutScreen();
  }

let allScreens = [];
allScreens.push(mainScreen);
allScreens.push(aboutScreen);

function hideAllScreensExcept(screenToShow) {
    for (var index = 0; index<allScreens.length; index++) {
        if (allScreens[index] == null) {
            logging.error(`hideAllScreensExcept NULL screen at index ${index}`);
        }
        allScreens[index].style.display = (allScreens[index] === screenToShow ? "inline" : "none");
    }
}

function showAboutScreen() {
    logging.debug("Show about screen");
    let timestamp = "";
    if (todoList_1.timestamp.length > 0) {
      timestamp = `List received\n${todoList_1.timestamp}\n`;
    }
    aboutTitle.text = `wrist-list\nv${literals.APP_VERSION}`;
    if (logging.getCurrentLogLevel() < logging.LOGGING_LEVEL_DEBUG) {
      // prod build
      aboutCopy.text = `${timestamp}\nprod-build`;
    } else {
      aboutCopy.text = `${timestamp}${deviceUtil.getJsMemoryStats()}\n${logging.getCurrentLogLevelLabel()}`;
    }
    hideAllScreensExcept(aboutScreen);
}
  
function showMainScreen() {
    logging.debug("Show main screen");
    hideAllScreensExcept(mainScreen);
}

// physical buttons

document.onkeypress = function(evt) {
  logging.debug(`onkeypress: ${JSON.stringify(evt)}`);
  if (evt.key === "back") {
      if (mainScreen.style.display === "inline") {
          // Default behaviour to exit the app
      } else {
          // Go to main screen
          showMainScreen();
          evt.preventDefault();
      }
  }
}


/* --------- UI ---------- */
setupTileList(todoList_1);

/* --------- CLOCK ---------- */
function clockCallback(data) {
  mainTitleRight.text = data.time;
}
simpleClock.initialize("minutes", clockCallback);

/* --------- SETTINGS ---------- */
function settingsCallback(receivedDataSet1) {
  todoList_1 = receivedDataSet1;
  setupTileList(todoList_1);
  refreshList(todoList_1);
}
try {
  deviceSettings.initialize(settingsCallback);
} catch (ex) {
  logging.error("failed to initialise settings");
  analytics.sendAnalyticsEvent(literals.ANALYTICS_CATEGORY_ERROR, literals.ANALYTICS_ACTION_LOAD_SETTINGS);
  deviceSettings.reset(settingsCallback);
}

/* --------- XFER ---------- */
function receiverCallback(data) {
  deviceUtil.logMemoryStats("receiverCallback - start");
  showMainScreen();
  todoList_1 = emptyObjects.default_list();

  updateList(data, todoList_1);
  deviceSettings.save(todoList_1);

  deviceUtil.logMemoryStats("receiverCallback - end");

  vibration.stop();
  vibration.start("nudge-max");
}
xfer.initialize(receiverCallback);

showMainScreen();
console.log(`wrist-list v${literals.APP_VERSION} started, ${me.buildId}, ${me.launchArguments}`);
deviceUtil.logMemoryStats("init complete");
