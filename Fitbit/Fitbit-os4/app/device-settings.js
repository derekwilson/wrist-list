/*
  persist the dataset into a CBOR file on the device
*/
import { me } from "appbit";
import * as fs from "fs";
import * as literals from "../common/literals";
import * as logging from "../common/logging";
import * as emptyObjects from "../common/objects";
import * as analytics from "./analytics";

const SETTINGS_TYPE = "cbor";
const DEVICE_SETTINGS_FILE = "devicesettings.cbor";
const defaultDeviceSettings = emptyObjects.default_list();

let currentDeviceSettings = defaultDeviceSettings;

export function initialize(callback) {
  currentDeviceSettings = loadDeviceSettings();
  logging.info(`device settings initialised length = ${currentDeviceSettings.list.length} `);
  checkForVersionUpdate();
  callback(currentDeviceSettings);
}

export function reset(callback) {
  currentDeviceSettings = defaultDeviceSettings;
  logging.info(`device settings reset length = ${currentDeviceSettings.list.length} `);
  callback(currentDeviceSettings);
}

export function save(data) {
  currentDeviceSettings = data;
  currentDeviceSettings[literals.SETTING_VERSION] = literals.APP_VERSION;
  saveDeviceSettings();
}

// Register for the unload event
me.addEventListener("unload", saveDeviceSettings);

// check if the version has changed
function checkForVersionUpdate() {
  var update = false;
  if (currentDeviceSettings[literals.SETTING_VERSION]) {
    // a version is specified
    update = currentDeviceSettings[literals.SETTING_VERSION] != literals.APP_VERSION;
  } else {
    update = true;
  }
  if (update) {
    logging.info(`version has changed from ${currentDeviceSettings[literals.SETTING_VERSION]} to ${literals.APP_VERSION}`);
    analytics.sendAnalyticsEventAndLabel(
      literals.ANALYTICS_CATEGORY_UPGRADE, 
      literals.APP_VERSION,
      currentDeviceSettings[literals.SETTING_VERSION]
    );
  }
  currentDeviceSettings[literals.SETTING_VERSION] = literals.APP_VERSION;
}

// Load settings from filesystem
function loadDeviceSettings() {
  try {
    return fs.readFileSync(DEVICE_SETTINGS_FILE, SETTINGS_TYPE);
  } catch (ex) {
    return defaultDeviceSettings;
  }
}

// Save settings to the filesystem
function saveDeviceSettings() {
  logging.info(`saving device settings length = ${currentDeviceSettings.list.length} `);
  //logging.debug("save device settings " + JSON.stringify(currentDeviceSettings));
  fs.writeFileSync(DEVICE_SETTINGS_FILE, currentDeviceSettings, SETTINGS_TYPE);
}
