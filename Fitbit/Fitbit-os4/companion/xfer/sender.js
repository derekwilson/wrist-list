import { encode } from "cbor";
import { outbox } from "file-transfer";
import * as logging from "../../common/logging";

export function sendDataToDevice(data) {
    var filename = "_wrist_list_todo";
    outbox.enqueue(filename, encode(data)).then(function () {
        logging.info("File: " + filename + " transferred successfully.");
    }).catch(function (error) {
        logging.error("File: " + filename + " failed to transfer.");
    });
}

